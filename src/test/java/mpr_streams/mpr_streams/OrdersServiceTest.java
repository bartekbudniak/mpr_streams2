package mpr_streams.mpr_streams;

import static org.assertj.core.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.AbstractIterableAssert;
import org.assertj.core.api.ListAssert;
import org.assertj.core.api.ObjectAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OrdersServiceTest {
	ClientDetails aClient;
	ClientDetails bClient;
	ClientDetails cClient;
	OrderItem item1;
	OrderItem item2;
	OrderItem item3;
	OrderItem item4;
	OrderItem item5;
	OrderItem item6;
	OrderItem item7;
	OrderItem item8;
	List<OrderItem> aItems;
	List<OrderItem> bItems;
	List<OrderItem> cItems;
	Order aOrder;
	Order bOrder;
	Order cOrder;
	List<Order> orders;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	@Before
	public void setup(){
		aClient = new ClientDetails("aLogin","aName","aSurname",15);
		bClient = new ClientDetails("bLogin","bName","bSurname",19);
		cClient = new ClientDetails("cLogin","SName","cSurname",21);
		item1 = new OrderItem(1,"a");
		item2 = new OrderItem(2,"b");
		item3 = new OrderItem(3,"c");
		item4 = new OrderItem(4,"d");
		item5 = new OrderItem(5,"de");
		item6 = new OrderItem(6,"e");
		item7 = new OrderItem(7,"f");
		item8 = new OrderItem(8,"g");
		aItems= Arrays.asList(item1, item2, item3, item4, item5);
		bItems= Arrays.asList(item1, item2, item3, item4, item5, item6, item7, item8);
		cItems= Arrays.asList(item1, item2, item3, item4);
		aOrder = new Order(aClient, aItems, "A to jest komentarz");
		bOrder = new Order(bClient, bItems, "Short");
		cOrder = new Order(cClient, cItems, "");
		orders = Arrays.asList(aOrder, bOrder, cOrder);
		System.setOut(new PrintStream(outContent));
	}
	
	/* 1 */
	@Test
	public void FindOrdersWhichHaveMoreThan5OrderItems(){
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		assertThat(result).containsOnly(bOrder);
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersWhichHaveMoreThan5OrderItems(){
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(Arrays.asList(aOrder));//kolekcja po sfiltorwaniu jest pusta
		assertThat(result.isEmpty()).isTrue();
	}
	
	@Test
	public void WhenThereIsNullListOfOrdersPassingToListOfOrdersWhichHaveMoreThan5OrderItems(){//null przekazany do metody
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(null);
		assertThat(result).isNull();
	}

	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToListOfOrdersWhichHaveMoreThan5OrderItems(){
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(new ArrayList<Order>());//pusta lista przekazana do metody
		assertThat(result.isEmpty()).isTrue();
	}
	
	/* 2 */
	@Test
	public void FindOldestClientAmongThoseWhoMadeOrders(){
		ClientDetails result = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		cClient.equals(assertThat(result));
	}
	
	public void WhenThereIsNullListOfOrdersPassingToFindOldestClientAmongThoseWhoMadeOrders(){
		ClientDetails result = OrdersService.findOldestClientAmongThoseWhoMadeOrders(null);
		assertThat(result).isNull();
	}
	
	public void WhenThereIsEmptyListOfOrdersPassingToFindOldestClientAmongThoseWhoMadeOrders(){
		ClientDetails result = OrdersService.findOldestClientAmongThoseWhoMadeOrders(new ArrayList<Order>());
		assertThat(result).isNull();
	}
	
	/* 3 */
	@Test
	public void FindOrderWithLongestComments() {
		Order result = OrdersService.findOrderWithLongestComments(orders);
		aOrder.equals(assertThat(result));
	}
	
	@Test
	public void WhenThereIsListWhitoutCommentPassingToFindOrderWithLongestComments() {
		Order result = OrdersService.findOrderWithLongestComments(Arrays.asList(cOrder));
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsNullListOfOrdersPassingToFindOrderWithLongestComments() {
		Order result = OrdersService.findOrderWithLongestComments(null);
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToFindOrderWithLongestComments() {
		Order result = OrdersService.findOrderWithLongestComments(new ArrayList<>());
		assertThat(result).isNull();
	}
	
	/* 4 */
	@Test
	public void GetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(){
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		assertThat(result).isEqualTo("bName bSurname; SName cSurname");
	}
	
	@Test
	public void WhenThereIsNullListOfOrdersPassingToGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOldTest(){
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(null);
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOldTest(){
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(new ArrayList<Order>());
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOldTest(){
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(Arrays.asList(aOrder));
		assertThat(result).isEqualTo("");
	}
	
	/* 5*/
	@Test
	public void GetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		assertThat(result).isEqualTo(Arrays.asList("a","b","c","d","de"));
	}
	
	@Test
	public void WhenThereIsNullListOfOrdersPassingToGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(null);
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(new ArrayList<Order>());
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(Arrays.asList(bOrder,cOrder));
		assertThat(result).isEmpty();
	}
	
	/* 6 */
	@Test
	public void PrintCapitalizedClientsLoginsWhoHasNameStartingWithS(){
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
		assertThat("CLOGIN").isEqualTo(outContent.toString());
	}

	@Test
	public void WhenThereIsNullListOfOrdersPassingToPrintCapitalizedClientsLoginsWhoHasNameStartingWithS(){
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(null);
		assertThat("").isEqualTo(outContent.toString());
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToPrintCapitalizedClientsLoginsWhoHasNameStartingWithS(){
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(new ArrayList<Order>());
		assertThat("").isEqualTo(outContent.toString());
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPrintCapitalizedClientsLoginsWhoHasNameStartingWithS(){
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(Arrays.asList(aOrder));
		assertThat("").isEqualTo(outContent.toString());
	}
	
	/* 7 */
	@Test
	public void GroupOrdersByClient(){
		Map<ClientDetails, List<Order>> whatShouldBe = new HashMap<ClientDetails, List<Order>>(); 
		whatShouldBe.put(aClient, Arrays.asList(aOrder));	
		whatShouldBe.put(bClient, Arrays.asList(bOrder));
		whatShouldBe.put(cClient, Arrays.asList(cOrder));			
		Map<ClientDetails, List<Order>> result = OrdersService.groupOrdersByClient(orders);
		assertThat(result).isEqualTo(whatShouldBe);
	}
	
	@Test
	public void WhenThereIsNullListOfOrdersPassingToGroupOrdersByClient(){
		Map<ClientDetails, List<Order>> result = OrdersService.groupOrdersByClient(null);
		assertThat(result).isNull();
	}
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToGroupOrdersByClient(){
		Map<ClientDetails, List<Order>> result = OrdersService.groupOrdersByClient(new ArrayList<Order>());
		assertThat(result).isNull();
	}
		
	/* 8 */
	@Test
	public void WhenThereIsNullListOfOrdersPassingToPartitionClientsByUnderAndOver18(){
		Map<Boolean, List<ClientDetails>> result = OrdersService.partitionClientsByUnderAndOver18(null);
		assertThat(result).isNull();
	}
	
	@Test
	public void WhenThereIsEmptyListOfOrdersPassingToPartitionClientsByUnderAndOver18(){
		Map<Boolean, List<ClientDetails>> result = OrdersService.partitionClientsByUnderAndOver18(new ArrayList<Order>());
		assertThat(result).isNull();
	}
	
	@Test
	public void PartitionClientsByUnderAndOver18(){
		Map<Boolean, List<ClientDetails>> whatShouldBe = new HashMap<>(); 
		whatShouldBe.put(false, Arrays.asList(aClient));	
		whatShouldBe.put(true, Arrays.asList(bClient, cClient));			
		Map<Boolean, List<ClientDetails>> result = OrdersService.partitionClientsByUnderAndOver18(orders);
		assertThat(result).isEqualTo(whatShouldBe);
	}
	
	@After
	public void clean(){
		System.setOut(null);
	}
}


	
	